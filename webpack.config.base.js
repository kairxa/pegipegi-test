var path = require('path');
var webpack = require('webpack');
var nodeDir = path.resolve(__dirname, 'node_modules');
var process = require('process');

var pegipegiPath = 'build/pegipegi.jsx';

var config = {
    entry: {
        pegipegi: path.resolve(__dirname, pegipegiPath),
        vendors: [
          'react',
          'react-dom'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist/assets/javascripts'),
        filename: '[name].js',
        publicPath: '/assets/javascripts/'
    },
    module: {
        loaders: [
            {
                test: /\.(jsx|js)$/,
                exclude: /(node_modules)/,
                loader: 'babel?presets[]=es2015'
            }
        ],
        preLoaders: [
            {
                test: /\.(js|jsx)?/,
                exclude: [nodeDir],
                loader: 'source-map-loader'
            }
        ]
    },
    plugins: [],
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".jsx", ".js"],
        modulesDirectories: ['web_modules', 'node_modules', 'build']
    }
};

module.exports = config;