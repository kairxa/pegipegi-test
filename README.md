# PegiPegi Technical Test

Hello all. This repo contains the technical test given yesterday by the PegiPegi team.  
The test consists of three different versions; one of them is the mandatory jQuery version.  
Each version can be loaded from three different html files.

1. `index.html`
  - This is the mandatory jQuery version.
  - jQuery is still a problematic concoction after two years of not using it.
  - There's a lot of 'fighting the library' moment because of preset styling from jQuery UI.
  - I have no idea why jQuery UI is not compatible with the latest 3.x release of jQuery.
  
2. `pure-js.html`
  - This is the autocomplete implementation in pure native javascript.
  - It was fun and annoying at the same time; best used for digging technical knowledge and skill of js devs.
  - Not to be used in production because native javascript tends to get messy fast, especially when dealing with dynamic elements.
  
3. `react.html`
  - This is the implementation using ReactJS library.
  - The fastest one to be created from scratch; about 30 minutes used minus the CSS after waking up this morning.
  - Arguably the easiest to maintain compared to others, minus the minuscule learning curve.
  
Everything is tested using `live-server` library from npm. If you want to build the ReactJS version, you can do `npm install` or `yarn` and run `npm run webpack`.

Thank you for the chance given and have a nice day :)