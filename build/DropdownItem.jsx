import React from 'react';

class DropdownItem extends React.Component {
  constructor(props) {
    super(props);

    const { data } = this.props;

    this.name = data.region ? `${data.name}, ${data.region}` : data.name;
    this.count = data.count ? `${data.count} hotels` : '';

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.handleItemClick(this.name);
  }

  render() {
    return (
      <li onClick={this.handleClick}>
        <div className="dropdown-link">
          <span className="dropdown-link-name">
            {this.name}
          </span>
          {this.count &&
          <div className="dropdown-link-count">{this.count}</div>  
          }
        </div>
      </li>
    );
  }
}

DropdownItem.propTypes = {
  data: React.PropTypes.shape({
    name: React.PropTypes.string,
    region: React.PropTypes.string,
    count: React.PropTypes.number
  }),
  handleItemClick: React.PropTypes.func,
};

export default DropdownItem;
