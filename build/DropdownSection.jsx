import React from 'react';

import DropdownItem from './DropdownItem';

const titleTable = window.pegipegiFeed.titleTable;

const DropdownSection = ({ sectionData, sectionName, handleItemClick }) => (
  <section className="dropdown-section">
    <div className="dropdown-section-title">
      <span>{titleTable[sectionName]}</span>
    </div>
    <ul className="dropdown-list">
      {sectionData.map((data, key) => (
        <DropdownItem data={data} key={key} handleItemClick={handleItemClick} />
      ))}
    </ul>
  </section>
);

DropdownSection.propTypes = {
  sectionData: React.PropTypes.array,
  sectionName: React.PropTypes.string,
  handleItemClick: React.PropTypes.func,
};

export default DropdownSection;
