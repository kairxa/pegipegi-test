import React from 'react';
import ReactDOM from 'react-dom';

import DropdownSection from './DropdownSection';

const initialFeed = window.pegipegiFeed.initialFeed;

class Autocomplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      usedFeed: {
        areas: [],
        regions: [],
        cities: [],
        hotelsNearby: [],
        hotelsRegistered: [],
      },
      inputValue: '',
      shouldShowReset: false,
      shouldShowDropdown: false,
    };

    this.handleFormReset = this.handleFormReset.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  handleFormReset() {
    this.setState({
      inputValue: '',
      shouldShowReset: false,
      shouldShowDropdown: false,
    });
  }

  handleInputChange(event) {
    if (event.target.value.trim()) {
      this.setState({
        usedFeed: window.pegipegiFeed.filteredFeed(initialFeed, event.target.value), // reference, do not mutate
        inputValue: event.target.value,
        shouldShowReset: true,
        shouldShowDropdown: true,
      });
    } else {
      this.setState({
        usedFeed: {
          areas: [],
          regions: [],
          cities: [],
          hotelsNearby: [],
          hotelsRegistered: [],
        },
        inputValue: event.target.value,
        shouldShowReset: false,
        shouldShowDropdown: false,
      });
    }
  }

  handleItemClick(inputValue) {
    this.setState({
      inputValue,
      shouldShowReset: true,
      shouldShowDropdown: false,
    });
  }

  render() {
    const resetButtonClass = this.state.shouldShowReset ? 'reset-button' : 'reset-button invisible';
    const dropdownClass = this.state.shouldShowDropdown ? 'dropdown' : 'dropdown invisible';

    return  (
      <form onReset={this.handleFormReset} className="input-container">
        <input
          type="text"
          className="input"
          onChange={this.handleInputChange}
          value={this.state.inputValue}
        />
        <button type="reset" className={resetButtonClass}>RESET</button>
        <div className={dropdownClass}>
          {this.state.usedFeed.areas.length > 0 &&
          <DropdownSection
            sectionData={this.state.usedFeed.areas}
            sectionName="areas"
            handleItemClick={this.handleItemClick}
          />
          }
          {this.state.usedFeed.regions.length > 0 &&
          <DropdownSection
            sectionData={this.state.usedFeed.regions}
            sectionName="regions"
            handleItemClick={this.handleItemClick}
          />
          }
          {this.state.usedFeed.cities.length > 0 &&
          <DropdownSection
            sectionData={this.state.usedFeed.cities}
            sectionName="cities"
            handleItemClick={this.handleItemClick}
          />
          }
          {this.state.usedFeed.hotelsNearby.length > 0 &&
          <DropdownSection
            sectionData={this.state.usedFeed.hotelsNearby}
            sectionName="hotelsNearby"
            handleItemClick={this.handleItemClick}
          />
          }
          {this.state.usedFeed.hotelsRegistered.length > 0 &&
          <DropdownSection
            sectionData={this.state.usedFeed.hotelsRegistered}
            sectionName="hotelsRegistered"
            handleItemClick={this.handleItemClick}
          />
          }
        </div>
      </form>
    )
  }
}

ReactDOM.render(
  <Autocomplete />,
  document.querySelector('#pegi-react')
);
