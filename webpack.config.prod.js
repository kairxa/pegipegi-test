var base = require('./webpack.config.base.js');
var webpack = require('webpack');
var path = require('path');
var nodeDir = path.resolve(__dirname, 'node_modules');

var config = Object.assign({}, base, {
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    })
  ]
});

module.exports = config;