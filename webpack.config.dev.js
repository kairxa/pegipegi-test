var base = require('./webpack.config.base.js');
var webpack = require('webpack');
var path = require('path');
var nodeDir = path.resolve(__dirname, 'node_modules');

var config = Object.assign({}, base, {
  devtool: 'source-map',
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
  ]
});

module.exports = config;