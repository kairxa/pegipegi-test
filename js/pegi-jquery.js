var pegipegiJq = $(document).ready(function() {
  $.widget( "custom.customautocomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
      });
    }
  });

  var feed = [
    {
      label: 'Ancol, Jakarta Utara',
      category: 'Area',
      value: ''
    },
    {
      label: 'Pantai Sambolo, Anyer',
      category: 'Area',
      value: ''
    },
    {
      label: 'Plaza Hotel Marco Mangga Dua Jakarta',
      category: 'Hotel Terdekat',
      value: ''
    },
    {
      label: 'Amaris Hotel Mangga Dua Square',
      category: 'Hotel Terdekat',
      value: ''
    },
    {
      label: 'Discovery Hotel & Convention Ancol Pulau Putri',
      category: 'Hotel Terdaftar',
      value: ''
    }
  ];

  var input = $('.input');
  var resetButton = $('.reset-button');
  var dropdown = $('.dropdown');
  var item = $('.ui-menu-item');

  resetButton.click(function() {
    resetButton.addClass('invisible');
    dropdown.addClass('invisible');
  });
  input.keyup(function() {
    if (input.val()) {
      resetButton.removeClass('invisible');
      dropdown.removeClass('invisible');
    } else {
      resetButton.addClass('invisible');
      dropdown.addClass('invisible');
    }
  });
  input.customautocomplete({
    appendTo: '.dropdown',
    source: feed,
    select: function() {
      dropdown.addClass('invisible');
    }
  });
});