var pegipegiFeed = (function() {
  var feed = {
    areas: [
      {
        name: 'Ancol',
        region: 'Jakarta Utara',
        city: 'Jakarta',
        count: 10
      },
      {
        name: 'Pantai Sambolo',
        region: 'Anyer',
        city: 'Banten',
        count: 15
      }
    ],
    regions: [
      {
        name: 'Jakarta Utara',
        city: 'Jakarta',
        count: 100
      },
      {
        name: 'Anyer',
        city: 'Banten',
        count: 150
      }
    ],
    cities: [
      {
        name: 'Jakarta',
        count: 1000
      },
      {
        name: 'Banten',
        count: 500
      }
    ],
    hotelsNearby: [
      {
        name: 'Discovery Hotel & Convention Ancol Pulau Putri'
      },
      {
        name: 'Plaza Hotel Marco Mangga Dua Jakarta'
      },
      {
        name: 'Amaris Hotel Mangga Dua Square'
      },
      {
        name: 'Random Hotel Name in Ancol'
      },
      {
        name: 'Quick Brown Hotel of Ancol'
      },
      {
        name: 'Jumps Over The Lazy Dog of Pantai Sambolo in Anyer Banten'
      },
      {
        name: 'Absurdly Written Hotel Name of Anyer in Banten'
      }
    ],
    hotelsRegistered: [
      {
        name: 'Discovery Hotel & Convention Ancol Pulau Putri'
      },
      {
        name: 'Plaza Hotel Marco Mangga Dua Jakarta'
      },
      {
        name: 'Amaris Hotel Mangga Dua Square'
      },
      {
        name: 'Random Hotel Name in Ancol'
      }
    ]
  };

  var titleTable = {
    areas: 'Area',
    regions: 'Region',
    cities: 'Kota',
    hotelsNearby: 'Hotel Terdekat',
    hotelsRegistered: 'Hotel Terdaftar'
  };

  var filterResult = function(query, object) {
    var regexp = new RegExp(query, 'gi');

    return object.name.match(regexp);
  };

  var filteredFeed = function(initialFeed, query) {
    var areas = initialFeed.areas.filter(filterResult.bind(this, query));
    var regions = initialFeed.regions.filter(filterResult.bind(this, query));
    var cities = initialFeed.cities.filter(filterResult.bind(this, query));
    var hotelsNearby = initialFeed.hotelsNearby.filter(filterResult.bind(this, query));
    var hotelsRegistered = initialFeed.hotelsRegistered.filter(filterResult.bind(this, query));

    var filtered = {
      areas: JSON.parse(JSON.stringify(areas)), // deep copying
      regions: JSON.parse(JSON.stringify(regions)),
      cities: JSON.parse(JSON.stringify(cities)),
      hotelsNearby: JSON.parse(JSON.stringify(hotelsNearby)),
      hotelsRegistered: JSON.parse(JSON.stringify(hotelsRegistered))
    };

    return filtered;
  };

  return {
    titleTable: JSON.parse(JSON.stringify(titleTable)),
    initialFeed: JSON.parse(JSON.stringify(feed)),
    filteredFeed
  };
})();