var pegipegiPureFunction = (function() {
  var form = document.querySelector('#purejs-form');
  var input = document.querySelector('#purejs-query-input');
  var resetButton = document.querySelector('#purejs-reset-button');
  var dropdown = document.querySelector('#purejs-dropdown');
  
  var toggleResetButton = function(event) {
    if (input.value) {
      resetButton.classList.remove('invisible');
      dropdown.classList.remove('invisible');
    }

    if (event.type === 'reset' || !input.value) {
      resetButton.classList.add('invisible');
      dropdown.classList.add('invisible');
    }
  };

  var handleLinkClick = function(inputValue) {
    input.value = inputValue;

    dropdown.classList.add('invisible');
  };

  var createLi = function(data, sectionName) {
    var li = document.createElement('li');
    var div = document.createElement('div');
    var linkName = document.createElement('span');
    var linkCount;

    linkName.classList.add('dropdown-link-name');

    if (sectionName === 'areas') {
      linkName.innerHTML = data.name + ', ' + data.region; 
    } else {
      linkName.innerHTML = data.name;
    }

    if (!sectionName.match(/(hotels)/gi)) {
      linkCount = document.createElement('div');
      linkCount.classList.add('dropdown-link-count');
      linkCount.innerHTML = data.count + ' hotels';
    }
    
    div.classList.add('dropdown-link');
    div.appendChild(linkName);
    
    if (linkCount) {
      div.appendChild(linkCount);
    }
    
    li.appendChild(div);
    li.addEventListener('click', handleLinkClick.bind(this, linkName.innerHTML));

    return li;
  };

  var dropdownFilter = function() {
    var sectionList = document.querySelectorAll('.dropdown-section');

    sectionList.forEach(function(node) {
      dropdown.removeChild(node);
    });

    if (input.value.trim()) {
      var feed = window.pegipegiFeed.filteredFeed(window.pegipegiFeed.initialFeed, input.value.trim());
      var sectionData;
      var section;
      var title;
      var ul;

      for (var sectionName in feed) {
        sectionData = feed[sectionName];

        if (sectionData.length > 0) {
          section = document.createElement('section');
          section.classList.add('dropdown-section');
          title = document.createElement('div');
          title.classList.add('dropdown-section-title');
          title.innerHTML = window.pegipegiFeed.titleTable[sectionName];
          ul = document.createElement('ul');
          ul.classList.add('dropdown-list');

          sectionData.forEach(function(data) {
            ul.appendChild(createLi(data, sectionName));
          });
          
          section.appendChild(title);
          section.appendChild(ul);
          dropdown.appendChild(section);
        }
      }
    }
  };
  
  input.addEventListener('keyup', toggleResetButton);
  input.addEventListener('keyup', dropdownFilter);
  form.addEventListener('reset', toggleResetButton);
})();